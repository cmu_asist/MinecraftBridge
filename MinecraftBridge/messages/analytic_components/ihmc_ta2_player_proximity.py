# -*- coding: utf-8 -*-
"""
.. module:: ihmc_ta2_dyad
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating IHMC Dyad measures

.. moduleauthor:: Noel Chen <yunhsua3@andrew.cmu.edu>

Definition of a class encapsulating IHMC Player Proximity measures.
"""

import json
import enum

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage

from collections import namedtuple


class IHMC_PlayerProximity(BaseMessage):
    """
    A class encapsulating player proximity messages.

    Attributes
    ----------
    elapsed_milliseconds : int
        the number of elapsed milliseconds since mission start
    participants : list of Proximity
        list of proximity objects for each participant
    """

    # Definition of lightweight class to store participant information
    ProximityParticipant = namedtuple("ProximityParticipant", ["participant_id", "callsign", "role",
     "current_location", "distance_to_participants",
    "distance_to_current_location_exits", "distance_to_closest_locations",
    "distance_to_role_change", "distance_to_treatment_areas"])

    # Definition of lightweight class to store Distance information
    Distance = namedtuple("id", "distance")


    def __init__(self, **kwargs):

        BaseMessage.__init__(self, **kwargs)

        # Check to see if the necessary arguments have been passed, raise an 
        # exception if one is missing
        for arg_name in ["elapsed_milliseconds", "participants"]:
            if not arg_name in kwargs:
                raise MissingMessageArgumentException(str(self), 
                                                      arg_name) from None

        # Make sure that `participants` are correctly formatted
        for participant in kwargs.get("participants", []):
            for arg_name in ["participant_id", "callsign", "role", "current_location",\
                             "distance_to_participants", "distance_to_current_location_exits",\
                             "distance_to_closest_locations", "distance_to_role_change",\
                             "distance_to_treatment_areas"]:
                if not arg_name in participant:
                    raise MalformedMessageCreationException(str(self), "participants",
                                                            kwargs["participants"]) from None


        # Populate the fields
        self._elapsed_milliseconds = kwargs["elapsed_milliseconds"]
        self._participants = [ IHMC_PlayerProximity.ProximityParticipant(p["participant_id"],
                                                                        p["callsign"],
                                                                        p["role"],
                                                                        p["current_location"],
                                                                        p["distance_to_participants"],
                                                                        p["distance_to_current_location_exits"],
                                                                        p["distance_to_closest_locations"],
                                                                        p["distance_to_role_change"],
                                                                        p["distance_to_treatment_areas"]) 
                               for p in kwargs.get("participants", []) ]


    @property
    def elapsed_milliseconds(self):
        """

        Attempting to set `elapsed_milliseconds` raises an `ImmutableAttributeException`
        """

        return self._elapsed_milliseconds

    @elapsed_milliseconds.setter
    def elapsed_milliseconds(self, _):

        raise ImmutableAttributeException(self, "elapsed_milliseconds")


    @property
    def participants(self):
        """

        Attempting to set `participants` raises an `ImmutableAttributeException`
        """

        return self._participants

    @participants.setter
    def participants(self, _):

        raise ImmutableAttributeException(self, "participants")


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'IHMC_Dyad')
        """

        return self.__class__.__name__


    def toDict(self):
        """
        Generates a dictionary representation of the Dyad message.
        Message information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the Dyad message.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the Cognitive Load data
        jsonDict["data"]["elapsed_milliseconds"] = self.elapsed_milliseconds
        jsonDict["data"]["participants"] = [ { "participant_id": p.participant_id,
                                               "callsign": p.callsign,
                                               "role": p.role,
                                                "current_location": p.current_location,
                                                "distance_to_participants": p.distance_to_participants,
                                                "distance_to_current_location_exits": p.distance_to_current_location_exits,
                                                "distance_to_closest_locations": p.distance_to_closest_locations,
                                                "distance_to_role_change": p.distance_to_role_change,
                                                "distance_to_treatment_areas": p.distance_to_treatment_areas }
                                              for p in self.participants ]

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the Dyad message.  
        Message information is contained in a JSON object under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            Dyad message.
        """

        return json.dumps(self.toDict())
